# ASPERA_REST



## Getting started

Proyecto de ejemplo servicios rest con Backend en Spring boot/MySQL y frontend con javascript

## Name
ASPERA_REST

## Description
Proyecto Java de ASPERA implementado en Spring Boot y Js, creando servicios Rest, lógica de negocio con servicios y repositorios JPA.

## Visuals
Aun no


## Support
anfelesvillada@gmail.com

## Project status
En Desarrollo
